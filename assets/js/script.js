//　文字コードUTF8
$(function(){
	$slider = $('.slider');
	
	if($slider[0]){
		$slider.slick({
			arrows: true,
			dots: true,
			fade: true,
			centerMode: true,
		});
	}
	
	$item_matchheight = $('.item');
	
	if($item_matchheight[0]){
	$item_matchheight.matchHeight();
	}//macthHeight
	
   // #で始まるアンカーをクリックした場合に処理
   $('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 600; // ミリ秒
       // 移動先を取得
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      // 移動先を数値で取得
      var position = target.offset().top;
      // スムーススクロール
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
});
